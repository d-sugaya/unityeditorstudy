﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Property : MonoBehaviour
{
    [SerializeField] private int attack;
    [SerializeField] private int deffence;
    // Inspectorで確認したい
    public int rank { 
        get {
            return attack * deffence;
        }
    }
}
