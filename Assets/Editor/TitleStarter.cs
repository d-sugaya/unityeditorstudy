﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

//=================================================================================================//
/// <summary>
/// タイトルからゲームを開始するエディタ拡張
/// </summary>
//=================================================================================================//
public class TitleStarter : Editor
{
    private const string StartScene = "Assets/Scenes/Title.unity";
    private const string SaveKey = "SaveScene";

    [MenuItem("Tools/Play From Title %@")]
    //=================================================================================================//
    /// <summary>
    /// タイトルから開始する
    /// ショートカット:Ctr+@
    /// </summary>
    //=================================================================================================//
    public static void PlayFromTitle()
    {
        // 保存しているかチェック
        if(EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
            PlayerPrefs.SetString(SaveKey, "Assets/Scenes/" + SceneManager.GetActiveScene ().name + ".unity");
            // シーンに遷移
            EditorSceneManager.OpenScene(StartScene);
            EditorApplication.isPlaying = true;
        }
    }

    [InitializeOnLoadMethod]
    //=================================================================================================//
    /// <summary>
    /// プレイを終了時に開始したシーンに復帰する
    /// </summary>
    //=================================================================================================//
    private static void gameEnd()
    {
        var scene = PlayerPrefs.GetString(SaveKey, "");
        if(string.IsNullOrEmpty(scene)) {
            return;
        }
        EditorApplication.playmodeStateChanged = () => {
            // 終了時に
            if(!EditorApplication.isPlaying) {
                // このイベントを削除
                EditorApplication.playmodeStateChanged = null;
                // 戻る
                scene = PlayerPrefs.GetString(SaveKey, "");
                PlayerPrefs.DeleteKey(SaveKey);
                EditorSceneManager.OpenScene(scene);
            }
        };
    }
}