﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerPrefsEditor : EditorWindow
{
    enum KeyType
    {
        Int,
        Float,
        String,
    }

    string key = "";
    string value = "";
    KeyType keyType;

    [MenuItem("Tools/PlayerPrefs/OpenEditor")]
    static void OpenEditor()
    {
        EditorWindow.GetWindow<PlayerPrefsEditor>("PlayrePrefsEditor");
    }

    // 呼ばれる基準
    // http://www.wisdomsoft.jp/258.html
    void OnGUI()
    {
        keyType = (KeyType)EditorGUILayout.EnumPopup("Type", keyType);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Key");
        key = GUILayout.TextField(key);
        GUILayout.Label("Value");
        value = GUILayout.TextField(value);
        GUILayout.EndHorizontal();

        switch(keyType) {
            case KeyType.Int: {
                    if(PlayerPrefs.HasKey(key)) {
                        var data = PlayerPrefs.GetInt(key);
                        GUILayout.Label("Saved Value: " + data.ToString());
                    }
                    if(GUILayout.Button("Save")) {
                        PlayerPrefs.SetInt(key, int.Parse(value));
                        PlayerPrefs.Save();
                    }
                    break;
                }
            case KeyType.Float: {
                    if(PlayerPrefs.HasKey(key)) {
                        var data = PlayerPrefs.GetFloat(key);
                        GUILayout.Label("Saved Value: " + data.ToString());
                    }
                    if(GUILayout.Button("Save")) {
                        PlayerPrefs.SetFloat(key, float.Parse(value));
                        PlayerPrefs.Save();
                    }
                    break;
                }
            case KeyType.String: {
                    if(PlayerPrefs.HasKey(key)) {
                        var data = PlayerPrefs.GetString(key);
                        GUILayout.Label("Saved Value: " + data.ToString());
                    }
                    if(GUILayout.Button("Save")) {
                        PlayerPrefs.SetString(key, value);
                        PlayerPrefs.Save();
                    }
                    break;
                }
        }
        if(GUILayout.Button("Delete")) {
            PlayerPrefs.DeleteKey(key);
            PlayerPrefs.Save();
        }
    }
}
