﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;

/// <summary>
/// http://q7z.hatenablog.com/entry/2016/07/06/223422
/// </summary>

[CanEditMultipleObjects]
[CustomEditor(typeof(Transform))]
public class EditorTransformUtility : Editor
{
    enum TargetType
    {
        Position,
        Rotation,
        Scale
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        var transform = target as Transform;

        DrawLine("P", TargetType.Position, transform);
        DrawLine("R", TargetType.Rotation, transform);
        DrawLine("S", TargetType.Scale, transform);

        serializedObject.ApplyModifiedProperties();
    }

    void DrawLine(string label, TargetType type, Transform transform)
    {
        Vector3 newValue = Vector3.zero;
        bool reset = false;

        EditorGUI.BeginChangeCheck();

        // Property
        using(new EditorGUILayout.HorizontalScope()) {
            if(GUILayout.Button(label, GUILayout.Width(20))) {
                newValue = type == TargetType.Scale ? Vector3.one : Vector3.zero;
                reset = true;
            }
            if(!reset) {
                switch(type) {
                    case TargetType.Position:
                        newValue = EditorGUILayout.Vector3Field("", transform.position, GUILayout.Height(16));
                        break;
                    case TargetType.Rotation:
                        newValue = EditorGUILayout.Vector3Field("", transform.localEulerAngles, GUILayout.Height(16));
                        break;
                    case TargetType.Scale:
                        newValue = EditorGUILayout.Vector3Field("", transform.localScale, GUILayout.Height(16));
                        break;
                    default:
                        Debug.Assert(false, "should not reach here");
                        break;
                }
            }
        }

        // Register Undo if changed
        if(EditorGUI.EndChangeCheck() || reset) {
            Undo.RecordObjects(targets, string.Format("{0} {1} {2}", (reset ? "Reset" : "Change"), transform.gameObject.name, type.ToString()));
            targets.ToList().ForEach(x => {
                var t = x as Transform;
                switch(type) {
                    case TargetType.Position:
                        t.position = newValue;
                        break;
                    case TargetType.Rotation:
                        t.localEulerAngles = newValue;
                        break;
                    case TargetType.Scale:
                        t.localScale = newValue;
                        break;
                    default:
                        Debug.Assert(false, "should not reach here");
                        break;
                }
                EditorUtility.SetDirty(x);
            });
        }
    }
}