﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Property))]
public class PropertyCustomEditor : Editor
{
    Property property = null;

    void OnEnable()
    {
        property = (Property)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        //攻撃力の数値をラベルとして表示する
        EditorGUILayout.LabelField("攻撃力", property.rank.ToString());
    }
}